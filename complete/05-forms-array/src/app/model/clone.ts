import { User } from './user';

export const cloneUser = (user: User): User => {
  const newUser = Object.assign({}, user);
  newUser.contacts = user.contacts.map(value => Object.assign({}, value));

  return newUser;
};
