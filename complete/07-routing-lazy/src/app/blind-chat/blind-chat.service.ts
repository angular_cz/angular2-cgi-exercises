import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ChatRoom, Message, Room } from '../chat/model/chat';
import { HttpClient } from '@angular/common/http';
import { map, share } from 'rxjs/operators';

@Injectable()
export class BlindChatService {

  private readonly ROOMS_URL = 'http://localhost:8000/rooms';

  private rooms$: Observable<Room[]>;

  constructor(private http: HttpClient) {
    this.rooms$ = this.http.get<Room[]>(this.ROOMS_URL).pipe(share());
  }

  getRooms(): Observable<Room[]> {
    return this.rooms$.pipe(
      map(rooms => {
        rooms.forEach(room => {
          room.title = getBlank(10);
          room.description = getBlank(100);
        });
        return rooms;
      }));
  }

  getRoomById(roomId: number): Observable<ChatRoom> {
    return this.http.get<ChatRoom>(this.ROOMS_URL + '/' + roomId);
  }

  sendMessage(roomId: number, message: Message): Observable<any> {
    return this.http.post(this.ROOMS_URL + '/' + roomId, message);
  }

}

function getBlank(size) {
  return new Array(size + 1).join('-');
}
