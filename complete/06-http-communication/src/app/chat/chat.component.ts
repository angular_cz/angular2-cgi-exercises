import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatRoom, Message } from '../model/chat';

import { interval, merge, Observable, Subject, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { startWith, flatMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  readonly CHATROOM_URL = 'http://localhost:8000/rooms/1';

  chatRoom: ChatRoom;

  myMessages$ = new Subject();
  myMessagesStream$: Observable<Response>;
  chatRoomStream$: Subscription;

  constructor(private http: HttpClient) {

    this.myMessagesStream$ = this.myMessages$
      .pipe(
        flatMap(message => this.http.post<Response>(this.CHATROOM_URL, message))
      );
  }

  ngOnInit(): void {

    this.chatRoomStream$ = merge(interval(4000), this.myMessagesStream$)
      .pipe(
        startWith(null),
        flatMap(() => this.http.get<ChatRoom>(this.CHATROOM_URL)),
        tap(() => console.log('chatReloaded')))
      .subscribe((chatRoom) => this.chatRoom = chatRoom);
  }

  sendMyMessage(message: Message) {
    this.myMessages$.next(message);
  }

  ngOnDestroy() {
    this.chatRoomStream$.unsubscribe();
  }
}
