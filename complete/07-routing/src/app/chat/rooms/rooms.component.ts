import { Component } from '@angular/core';
import { ChatService } from '../chat.service';
import { Room } from '../model/chat';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html'
})
export class RoomsComponent {

  rooms: Room[];

  constructor(route: ActivatedRoute) {

    this.rooms = route.snapshot.data.rooms;
  }

}
