import { Component, forwardRef } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  ControlValueAccessor,
  FormBuilder,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';
import { of, timer } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-contact-form',
  templateUrl: './user-contact-form.component.html',
  styleUrls: ['./user-contact-form.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UserContactFormComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => UserContactFormComponent),
      multi: true
    }
  ]
})
export class UserContactFormComponent implements ControlValueAccessor, Validator {

  contactForm = this.formBuilder.group({
    phone: ['', [Validators.required, Validators.minLength(9)]],
    email: ['', [Validators.required, Validators.email], contactDomainVaidator]
  });

  constructor(private formBuilder: FormBuilder) {
  }

  public onTouched: () => void = () => {
  }

  writeValue(value: any): void {
    value && this.contactForm.patchValue(value, {emitEvent: false});
  }

  registerOnChange(fn: any): void {
    this.contactForm.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.contactForm.disable() : this.contactForm.enable();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    return this.contactForm.valid ? null : {contact: {valid: false}};
  }
}

const contactDomainVaidator: AsyncValidatorFn = control => {

  if (!control || !control.value) {
    return of(null);
  }

  return timer(1000)
    .pipe(
      map(() => {
          if (control.value.endsWith('.cz')) {
            return null;
          }

          return {
            serverMessage: {
              message: `Only CZ domains are allowed`
            }
          };
        }
      )
    );
};
